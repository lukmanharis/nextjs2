import { useReducer, useEffect, useRef } from "react";
import { useRouter } from 'next/router'
import firebase from "./firebase";
import localStorage from "./LocalStorage";

const firestore = firebase.firestore();

function get_purchases_by_userId(user_id){
    firestore.collection("purchases").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            if (doc.data.user_id == user_id){
                localStorage.add_local_purchase(doc.data())
            }
        })
    })
}

function get_purchase(id){
    const router = useRouter();
    var id = router.query.id;
    console.log('test' + id);
    var docRef = firestore.collection("purchases").doc(id);
    docRef.get().then((doc) => {
    if (doc.exists) {
        console.log("Document data:", doc.data());
    } else {
        // doc.data() will be undefined in this case
        console.log("No such document!" + id);
    }
}).catch((error) => {
    console.log("Error getting document:", error);
}); 
}

function add_purchase(created_at = null, order_notes= null, products = null, total = null, user_id = null){
    firestore.collection("purchases").add({
        created_at: created_at,
        order_notes: order_notes,
        products: products,
        total: total,
        user_id: user_id,
    })
    .then((docRef) => {
        window.location = '/purchase_page/?id=' + docRef.id;
        console.log("Purchase written with user_ID: ", docRef.id);
    })
    .catch((error) => {
        console.error("Error adding purchase: ", error);
    });
}

function add_email(email = null, phonenumber = null, shipping_address = null){
    firestore.collection("users").add({
        email: email,
        phonenumber: phonenumber,
        shipping_address: shipping_address,
    })
    .catch((error) => {
        console.error("Error adding email: ", error);
    });
}


function remove_purchase(id){
    firestore.collection("purchases").get()
    .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            if (doc.id == id){
                console.log(id);
                firestore.collection("purchases").doc(doc.id).delete()
                .then((docRef) => {
                    location.reload();
                    console.log("Purchase deleted with user_ID: ", id);
                })
                .catch((error) => {
                    console.error("Error deleting purchase: ", error);
                });
            }
        })
    })
}

const purchase = {get_purchase, get_purchases_by_userId, add_purchase, remove_purchase, add_email}

export default purchase;