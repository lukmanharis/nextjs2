

function setSmsService(data){
    window.localStorage.setItem("Service_sid",JSON.stringify(data))
}

function getSidService(){
    var sid = JSON.parse(window.localStorage.getItem('Service_sid'));
    return sid.sid;
}

function verification_check(data){
    window.localStorage.setItem("verification_data",JSON.stringify(data))
}

function get_verification_data(){
    return JSON.parse(window.localStorage.getItem("verification_data"))
}

function add_products(data){
    window.localStorage.setItem("products",JSON.stringify(data))
}

function get_products(){
    return JSON.parse(window.localStorage.getItem("products"))
}

function add_single_product(data){
    window.localStorage.setItem("single_product",JSON.stringify(data))
}

function get_single_product(){
    return JSON.parse(window.localStorage.getItem("single_product"))
}

function remove_all_product(){
    window.localStorage.removeItem("products");
}


function get_purchase_by_userId(){
    return JSON.parse(window.localStorage.getItem("purchases"))
}

function add_local_purchase(data){
    window.localStorage.setItem("purchases",JSON.stringify(data))
}

function remove_local_purchase(){
    window.localStorage.removeItem("purchases");
}

const localStorage = {setSmsService,getSidService,verification_check,get_verification_data,add_products,get_products,add_single_product,get_single_product,remove_all_product, get_purchase_by_userId, add_local_purchase, remove_local_purchase}

export default localStorage;
