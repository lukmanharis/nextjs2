import axios from "axios";
import localStorage from "./LocalStorage";

var qs = require('qs');
const accountSid = process.env.NEXT_PUBLIC_TWILIO_ACCOUNT_SID;
const authToken = process.env.NEXT_PUBLIC_TWILIO_AUTH_TOKEN;
const email_Sid = process.env.NEXT_PUBLIC_EMAIL_VA_NUMBER;
const headers = {
auth:{
    username: accountSid,
    password: authToken
}}
function create_verify_number(user){
    var data = qs.stringify({
        'FriendlyName': 'phone number verifications' 
      });
    axios.post("https://verify.twilio.com/v2/Services", data, headers).then(function(response){
        localStorage.setSmsService(response.data);   
          var phone_data = qs.stringify({
            'To': user,
            'Channel': 'sms',
          });
            axios.post("https://verify.twilio.com/v2/Services/"+response.data.sid+"/Verifications",phone_data,headers).then(function(res){
                console.log(res)
            })

      
    });  
}

function sendemail(email){
  var email = qs.stringify({
    'To': email,
    'Channel': 'email' 
  });
  axios.post("https://verify.twilio.com/v2/Services/"+email_Sid+"/Verifications",email,headers).then(function(res){
    console.log(res)
  });
}

function resendcode(user,channel){
  console.log(email_Sid);
  if (channel == 'sms'){
    var data = qs.stringify({
      'To' : user,
      'Channel': channel
    });
    axios.post("https://verify.twilio.com/v2/Services/"+localStorage.getSidService()+"/Verifications",data,headers).then(function(res){
        console.log("test resend");
    })
  }else{
    var email = qs.stringify({
      'To': user,
      'Channel': 'email' 
    });
    axios.post("https://verify.twilio.com/v2/Services/"+email_Sid+"/Verifications",email,headers).then(function(res){
      console.log(res)
    });
  }
}

function verification_check(user,code,channel){
  console.log(channel);
  if(channel == 'sms'){
    var data = qs.stringify({
        'To': user,
        'Code': code 
      });
      axios.post("https://verify.twilio.com/v2/Services/"+localStorage.getSidService()+"/VerificationCheck",data,headers).then(function(res){
          const error = document.getElementById("error-message");
          localStorage.verification_check(res.data);
          if (res.data.valid == true){
            error.textContent = ""
            document.getElementsByClassName("code_input")[0].classList.remove("is-invalid");
          }else{
            document.getElementsByClassName("code_input")[0].classList.add("is-invalid");
            error.textContent = "Oops, your OTP is incorrect"
          }
      })
    }else{
      var data = qs.stringify({
        'To': user,
        'Code' : code
      })
      axios.post("https://verify.twilio.com/v2/Services/"+email_Sid+"/VerificationCheck",data,headers).then(function(res){
        const error = document.getElementById("error-message");
        localStorage.verification_check(res.data);
        if (res.data.valid == true){
          error.textContent = ""
          document.getElementsByClassName("code_input")[0].classList.remove("is-invalid");
        }else{
          document.getElementsByClassName("code_input")[0].classList.add("is-invalid");
          error.textContent = "Oops, your OTP is incorrect"
        }
      });
    }
}


const verify_number = { create_verify_number, verification_check ,sendemail,resendcode};

export default verify_number;