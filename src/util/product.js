import { useReducer, useEffect, useRef } from "react";
import firebase from "./firebase";
import localStorage from "./LocalStorage";

const firestore = firebase.firestore();

function get_product(){
    var data = [];
    firestore.collection("products").get().then((querySnapshot) => {
        if(querySnapshot.docs.length === 0){
            localStorage.remove_all_product()
        }else{
        querySnapshot.forEach((doc) => {
            data.push({data: doc.data() , id: doc.id})
            localStorage.add_products(data)
        });
    }
    });   
}

function add_single_product(id){
    firestore.collection("products").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            if (doc.id == id){
                console.log(id);
                localStorage.add_single_product(doc.data())
            }
        })
    })
}

function add_product(image = null, name = null, price = null){
    firestore.collection("products").add({
        product_image: image,
        product_name: name,
        product_price: price,
        stock: 20,
    })
    .then((docRef) => {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch((error) => {
        console.error("Error adding document: ", error);
    });
    get_product()
}

function remove_product(id){
    firestore.collection("products").get()
    .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            if (doc.id == id){
                console.log(id);
                firestore.collection("products").doc(doc.id).delete()
                .then((docRef) => {
                    location.reload();
                    console.log("Document deleted with ID: ", id);
                })
                .catch((error) => {
                    console.error("Error deleting document: ", error);
                });
                get_product();
            }
        })
    })
}

const product = {get_product,add_single_product, add_product, remove_product}

export default product;