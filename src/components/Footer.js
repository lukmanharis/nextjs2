import React from "react";
import Section from "components/Section";
import Container from "react-bootstrap/Container";
import Link from "next/link";
import "components/Footer.scss";
import Button from "react-bootstrap/Button";

function Footer(props) { 
  return (
    
    <Section
      bg={props.bg}
      textColor={props.textColor}
      size={props.size}
      bgImage={props.bgImage}
      bgImageOpacity={props.bgImageOpacity}
      className="footer"
    >
      <Container>
        <div className="FooterComponent__inner">
        <Button type="submit" className="w-75 verify-btn m-auto mb-5 fixed-bottom" variant="secondary" disabled>
            Send
        </Button>
        </div>
      </Container>
    </Section>
  );
}

export default Footer;
