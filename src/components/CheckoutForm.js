import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import React from "react";

const CheckoutForm = ({ paymentIntent }) => {
  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const {
        error,
        paymentIntent: { status },
      } = await stripe.confirmCardPayment(paymentIntent.client_secret, {
        payment_method: {
          card: elements.getElement(CardElement),
        },
      });

      if (error) throw new Error(error.message);

      if (status === "succeeded") {
        alert("success");
      }
    } catch (err) {
      return console.log(paymentIntent.client_secret);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <CardElement />

      <button type="submit" disabled={!stripe}>
        Pay now
      </button>
    </form>
  );
};

export default CheckoutForm;
