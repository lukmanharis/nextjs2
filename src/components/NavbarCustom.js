import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Link from "next/link";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Dropdown from "react-bootstrap/Dropdown";
import { useAuth } from "util/auth.js";

function NavbarCustom(props) {
  const auth = useAuth();
  if (typeof window !== "undefined") {
    
    var logout = document.getElementsByClassName("logout")[0];
    logout.onclick = function(){
      const response = fetch("/api/logout", {
        method:"GET",
        headers: {"Content-Type": "application/json"},
      }).then(function(res){
        if(res.ok == true){
          if (typeof window !== "undefined") {
            window.location.href = "/"
          }
        }
      });
    }

  }
  return (
    <Navbar bg={props.bg} variant={props.variant} expand={props.expand}>
      <Container>
        {/* <Link href="/" passHref={true}>
          <Navbar.Brand>
            <img
              className="d-inline-block align-top"
              src={props.logo}
              alt="Logo"
              height="30"
            />
          </Navbar.Brand>
        </Link> */}

        <Navbar.Toggle aria-controls="navbar-nav" className={props.btntoggle} />
        <div className={props.arrow}>
        <Link className="m-auto" href="/">
        <span><i class="fas fa-angle-left fa-3x"></i></span>
        </Link>
        <h3 className="m-auto"><strong>{props.middletext}</strong></h3>
        </div>
        <Link href="add_product">
        <button className={props.addbtn}>Add Product</button>
        </Link>
        <Navbar.Collapse id="navbar-nav" className="justify-content-end">
          <Nav>
           
             <Nav.Item>
               <Link href="/" passHref={true}>
                  <Nav.Link active={false}>Home</Nav.Link>
                </Link>
                <Link href="/checkout_page" passHref={true}>
                  <Nav.Link active={false}>Checkout</Nav.Link>
                </Link>
                <Link href="/saveinformation" passHref={true}>
                  <Nav.Link active={false}>SaveInformation</Nav.Link>
                </Link>
                <Link href="/purchase_page" passHref={true}>
                  <Nav.Link active={false}>Purchase</Nav.Link>
                </Link>
                <Link href="#" passHref={true}>
                  <Nav.Link className={props.user ? 'logout' : 'logout d-none'} active={false}>Log Out</Nav.Link>
                </Link>
             </Nav.Item>

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavbarCustom;
