import React from "react";
import Section from "components/Section";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SectionHeader from "components/SectionHeader";
import Button from "react-bootstrap/Button";
import Link from "next/link";
import Image from "react-bootstrap/Image";
import "components/HeroSection.scss";
import Card from "react-bootstrap/Card";
function HeroSection(props) {
  return (
    <Section
      bg={props.bg}
      textColor={props.textColor}
      size={props.size}
      bgImage={props.bgImage}
      bgImageOpacity={props.bgImageOpacity}
    >
      {!props.user && (
        <Container>
        <Row className="align-items-center">
          <Col lg={5} className="text-center text-lg-left">
            <SectionHeader
              title={props.title}
              subtitle={props.subtitle}
              size={1}
              spaced={false}
            />
            <Card>
              <Image className="thumbsnails-custom" src="/assets/img/greybackground.png" height={180} fluid={true}/>
                <Card.Body className="p-1 text-left">
                  <small>Welcome</small>
                  <Card.Text className="mt-2"><h2>Are you new?</h2></Card.Text>
                  <small>Click sign up and gain access to dashboard now to add products</small>
                </Card.Body>
                <Card.Footer className="mr-5 border-top-0 p-1">
                <Link href="/signup" passHref={true}>
                  <Button className="float-left w-50" variant={props.buttonColor}>
                    Sign up
                  </Button>
                </Link>
                </Card.Footer>
              </Card>
          </Col>
        </Row>
      </Container>
      )}
      {props.user && (
         <Container>
         <Row className="align-items-center">
           <Col lg={5} className="text-center text-lg-left">
             <SectionHeader
               title={props.title}
               subtitle={props.subtitle}
               size={1}
               spaced={false}
             />
             <Card>
               <Image className="thumbsnails-custom" src="/assets/img/greybackground.png" height={180} fluid={true}/>
                 <Card.Body className="p-1 text-left">
                   <small>{props.product_count ? props.product_count +" products" : "0 products"} </small>
                   <Card.Text className="mt-2"><h2>{props.product_count ? "You have " + props.product_count+ " products" : "You have no products"}</h2></Card.Text>
                   <small>tap on "add product" to add new products</small>
                 </Card.Body>
                 <Card.Footer className="mr-5 d-inline-flex border-top-0 p-1">
                 <Link href="/add_product" passHref={true}>
                   <Button className="float-left mr-1 w-50" variant={props.buttonColor}>
                     Add Products
                   </Button>
                 </Link>
                  <Link href="/list" passHref={true}>
                  <Button className="float-left w-50" variant={props.buttonColor}>
                     My Products
                   </Button>
                  </Link>
                 </Card.Footer>
               </Card>
           </Col>
         </Row>
       </Container>
      )}
      
    </Section>
  );
}

export default HeroSection;
