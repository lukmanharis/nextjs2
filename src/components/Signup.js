import React, { useState,useEffect,useRef } from "react";
import Form from "react-bootstrap/Form";
import FormField from "components/FormField";
import { useAuth } from "util/auth.js";
import { useForm } from "react-hook-form";
import Container from "react-bootstrap/Container";
import Section from "components/Section";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import IntlTelInput from "react-intl-tel-input";
import Footer from "components/Footer";
import twilio from "util/twillio";
import { useRouter } from 'next/router'
import https from "https";

var phone_data;
function SignupForm(props) {
  const auth = useAuth();
  const router = useRouter()
  const [pending, setPending] = useState(false);
  const { handleSubmit, register, errors, getValues } = useForm();
  
  function placeholder(){
    return "Phone Number"
  }

  const onPhoneNumberChange = (isValid, inputVal, countryDetails, number) => {
  phone_data = number.replace(/ /g,"");
  phone_data = phone_data.replace(/-/g,"");

  if (inputVal != ""){
    document.getElementsByClassName("verify-btn")[0].removeAttribute("disabled");
    document.getElementsByClassName("verify-btn")[0].classList.add("btn-primary");
    document.getElementsByClassName("verify-btn")[0].classList.remove('btn-secondary')
    document.getElementsByClassName("email_input")[0].setAttribute("disabled","disabled");
   }else{
     document.getElementsByClassName("verify-btn")[0].setAttribute("disabled","disabled");
     document.getElementsByClassName("verify-btn")[0].classList.add("btn-secondary");
     document.getElementsByClassName("verify-btn")[0].classList.remove('btn-primary');
     document.getElementsByClassName("email_input")[0].removeAttribute("disabled");
   }
  }
 
  const registerUser = event => {
    event.preventDefault() // don't redirect the page
    // where we'll add our form logic
    if (event.target.phone_number.value != ''){
        twilio.create_verify_number(phone_data,'sms');
        router.push('/phone_verify/'+phone_data);
    }else if(event.target.email.value != ''){
        twilio.sendemail(event.target.email.value);
        router.push('/phone_verify/'+event.target.email.value);

    }
   
  }

  return (
    <Section bg="white">
    <Container>
    <Form onSubmit={registerUser}>
        <div className="p-3 m-auto">
            <h4><strong className="font-weight-bold">Authenticate</strong></h4>
            <h6><span>Enter your phone number or email to get verification code for password reset</span></h6>
      
      <Row className="justify-content-center">
      <Col xs={12} md={12} className="py-3">
        <Form.Group controlId="phone">
        <IntlTelInput
            containerClassName="intl-tel-input w-100"
            inputClassName="form-control phone-number w-100"
            fieldName="phone_number"
            fieldId="phone_number"
            customPlaceholder={placeholder}
            defaultCountry="sg"
            onPhoneNumberChange={onPhoneNumberChange}
            
            />
        </Form.Group>
        </Col>
        </Row>
        <Row className="justify-content-center">
        <Col xs={12} md={12} className="py-3">
          <h6 className="lines-with-text"><span>OR</span></h6>
        </Col>
        </Row>
        <Row className="justify-content-center">
        <Col xs={12} md={12} className="py-3">
          <Form.Group controlId="formEmail">
            <FormField
              size="lg"
              name="email"
              type="email"
              placeholder="Email"
              className="email_input"
              error={errors.email}
              onChange={async (e) => {
                const { value } = e.currentTarget
                if (e.target.value != ""){
                 document.getElementsByClassName("verify-btn")[0].removeAttribute("disabled");
                 document.getElementsByClassName("verify-btn")[0].classList.add("btn-primary");
                 document.getElementsByClassName("verify-btn")[0].classList.remove('btn-secondary')
                 document.getElementsByClassName("phone-number")[0].setAttribute("disabled","disabled");
                }else{
                  document.getElementsByClassName("verify-btn")[0].setAttribute("disabled","disabled");
                  document.getElementsByClassName("verify-btn")[0].classList.add("btn-secondary");
                  document.getElementsByClassName("verify-btn")[0].classList.remove('btn-primary')
                  document.getElementsByClassName("phone-number")[0].removeAttribute("disabled");
                }
      
              }}
              inputRef={register({
                required: "Please enter an email",
              })}
            />
          </Form.Group>
        </Col>
        </Row>
        <Footer
          
        />
        </div>
    </Form>
    </Container>
    </Section>
  );
}

export default SignupForm;
