import React, { useState,useEffect,useRef } from "react";
import Form from "react-bootstrap/Form";
import FormField from "components/FormField";
import { useAuth } from "util/auth.js";
import Container from "react-bootstrap/Container";
import Section from "components/Section";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import IntlTelInput from "react-intl-tel-input";
import Footer from "components/Footer";
import twilio from "util/twillio";
import Button from "react-bootstrap/Button";
import localStorage from "../util/LocalStorage";
import { withIronSession } from "next-iron-session";
import { useRouter } from 'next/router'

function  SignupForm(props) {
  const auth = useAuth();
  const router = useRouter();
  function ValidateEmail(mail) 
    {
     if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail))
      {
        return true
      }
        return false
    }
  var countdownNum = 60;
  var countdown;
  const PhoneVerify = event => {
    event.preventDefault() // don't redirect the page
    // where we'll add our form logic
    var code = event.target.code.value;
    var user = props.user;
    console.log(user);
    const email = ValidateEmail(user);
    if (email != true){
      twilio.verification_check(user,code,"sms");
    }else{
      twilio.verification_check(user,code,"email")
    }
    setTimeout (function(){
      var verification_data = localStorage.get_verification_data()
      console.log(verification_data.valid)
      if (verification_data.valid == true){
        const response = fetch("/api/login", {
            method:"POST",
            headers: {"Content-Type": "application/json"},
            body : JSON.stringify({user})
          }).then(function(res){
            if(res.ok == true){
              if (typeof window !== "undefined") {
                window.location.href = "/"
              }
            }
          });
      }
    },2000)
    
    
  }
  
  function enablebutton(){
    setTimeout (function(){
      document.getElementById('resend-btn').removeAttribute("disabled");
      document.getElementById('resend-btn').classList.remove('btn-secondary');
      document.getElementById('resend-btn').style.background="#55d0f6"
      document.getElementById('resend-btn').style.color="white"
      },61000);
    }

    function incTimer(){
      setTimeout (function(){
          if(countdownNum != 0){
            countdownNum--;
            countdown.textContent = '('+countdownNum + 's)';
            incTimer();
          } else {
            countdown.textContent = '';
          }
      },1000);
    }

  const resend_code = event => {
    const email_validate = ValidateEmail(props.user);
    if(email_validate != true){
      twilio.resendcode(props.user,"sms");
    }else{
      twilio.resendcode(props.user,"email");
    }
    
    document.getElementById('resend-btn').setAttribute("disabled","disabled");
    document.getElementById('resend-btn').classList.add("btn-secondary");
    enablebutton()
    countdownNum = 60
    incTimer();
    
  }

  useEffect(() => {
    countdown = document.getElementById("timeleft");
    document.getElementById('resend-btn').setAttribute("disabled","disabled");
    enablebutton()
    incTimer();
 
  })

  return (
    <Section bg="white">
    <Container>
    <Form onSubmit={PhoneVerify}>
        <div className="p-3 m-auto">      
        <h4><strong className="font-weight-bold">Verify Now</strong></h4>
        <h6><span>we've sent 6 digit OTP code to (phone or email)</span></h6>
        <Row className="justify-content-center">
        <Col xs={12} md={12} className="py-3">
          <Form.Group controlId="formEmail">
            <FormField
              size="lg"
              name="code"
              type="text"
              placeholder="Code"
              className="code_input"
              onChange={async (e) => {
                const { value } = e.currentTarget
                if (e.target.value != ""){
                 document.getElementsByClassName("verify-btn")[0].removeAttribute("disabled");
                 document.getElementsByClassName("verify-btn")[0].classList.add("btn-primary");
                 document.getElementsByClassName("verify-btn")[0].classList.remove('btn-secondary')
                }else{
                  document.getElementsByClassName("verify-btn")[0].setAttribute("disabled","disabled");
                  document.getElementsByClassName("verify-btn")[0].classList.add("btn-secondary");
                  document.getElementsByClassName("verify-btn")[0].classList.remove('btn-primary')
                }
      
              }}
            />
            <span id="error-message" className="error-message text-danger"></span>
          </Form.Group>
        </Col>
        </Row>
        <Row className="justify-content-center">
          <Col xs={12} md={12} className="py-3 text-center">
            <Button variant="secondary" id="resend-btn" onClick={resend_code} >
              Resend <span id="timeleft"></span>
            </Button>
          </Col>
        </Row>
        <Footer
          
        />
        </div>
    </Form>
    </Container>
    </Section>
  );
}

export default SignupForm;
