import React from "react";
import Link from "next/link";
import product from "../util/product";
import purchase from "../util/purchase";
import localStorage from "../util/LocalStorage";
import "styles/global.scss";
import "../../public/assets/css/style.css";
import Moment from 'moment';

function checkoutPage (){
    product.get_product();
      var data_list = [];
      if (typeof window !== "undefined") {
        data_list = localStorage.get_products();
        console.log(data_list);
      }
      var sub_total = 0;
      data_list.map((item, index) => {
      sub_total += parseFloat(item.data.product_price);
      })

    const add_purchase = async (event) => {
        event.preventDefault();
        console.log("add purchase");
        var total = document.getElementById("total").value;
        var created_at = Moment(Date.now()).toString();
        purchase.add_purchase(created_at, null, null, total, null);
      };
      
    return (
        <>
        <div class="appHeader bg-white text-dark">
        <div class="left">
            <a href="#" class="headerButton goBack">
                <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
            </a>
        <div class="pageTitle">Checkout</div>
        </div>
        </div>

        <p style={{marginTop:'70px', textAlign: 'center', color: 'black'}}>Welcome back, (+email)</p>
        <div class="section mt-2">

        {data_list && data_list.map((item, index) => 
            <div class="card cart-item mb-2">
                <div class="card-body">
                    <div class="in">
                        {item.data.product_image.split(",",1).map((image)=>
                        <img src={image.substr(7)} alt="Image" class="imaged w64"></img>
                        )}
                        <div class="text">
                            <h3 class="title"> {item.data.product_name}</h3>
                            <p class="detail">1 piece</p>
                            <strong class="price">$ {item.data.product_price}</strong>
                        </div>
                    </div>
                    <div class="cart-item-footer">
                        <div class="stepper stepper-sm stepper-secondary">
                            <a class="stepper-button stepper-down">-</a>
                            <input id="ammount" type="text" class={"form-control input-"+item.id} value="1" readOnly=""></input>
                            <a class="stepper-button stepper-up" onClick={function(){
                                var input = document.getElementsByClassName("input-"+item.id)[0];
                                input.value++
                            }}>+</a>
                        </div>
                        <a href="#" class="btn btn-outline-secondary btn-sm" style={{float:'left'}}>Delete</a>
                    </div>
                </div>
            </div>
            )}
        </div>

        <div class="section mt-2 mb-2">
            <div class="card">
                <ul class="listview flush transparent simple-listview">
                    <li>SubTotal<span id="total"class="text-primary font-weight-bold">$ {sub_total}</span></li>
                </ul>
            </div>
        </div>

        <div class="section mb-2">
            <a href="/purchase_page" class="btn btn-primary btn-block" onClick={add_purchase}>Pay with card ending</a>
        </div>

        </>
    )
}

export default checkoutPage;