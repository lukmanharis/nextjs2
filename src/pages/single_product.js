import React from "react";
import Link from "next/link";
import firebase from "firebase/app";
import "styles/global.scss";
import "../../public/assets/css/style.css";

import { Splide, SplideSlide } from 'splide-nextjs/react-splide';
import localStorage from "../util/LocalStorage";
function SingleProductPage(props){
    var product = [];
    var i = 0;
    if (typeof window !== "undefined") {
        setTimeout(function(){
        var arrow = document.getElementsByClassName("splide__arrows")[0];
        arrow.classList.add("d-none")
        },300)
        product = localStorage.get_single_product()
        var image = document.getElementsByClassName("imaged")[0];
        product.product_image.split(",").map((Image)=>{
            document.getElementsByClassName("imaged")[i].src = Image.substr(7);
            document.getElementsByClassName("image-slide")[i].classList.remove("d-none");
            i++
        })
    }
     return (
        <>
        <div class="appHeader bg-primary text-light">
            <div class="left">
                <a href="/list" class="headerButton goBack">
                    <span><i class="fas fa-chevron-left fa-1x"></i></span>
                </a>
            </div>
        <div class="pageTitle">{product.product_name}</div>
        </div>
        <div class="appCapsule mt-3">
            <Splide className="mt-5 image">
            <SplideSlide className="image-slide d-none">
                <img alt="alt" class="imaged w-100 square"></img> 
            </SplideSlide>
            <SplideSlide className="image-slide d-none">
                <img alt="alt" class="imaged w-100 square"></img> 
            </SplideSlide>
            <SplideSlide className="image-slide d-none">
                <img alt="alt" class="imaged w-100 square"></img> 
            </SplideSlide>
            </Splide>
        <div class="section full">
        <div class="wide-block pt-2 pb-2 product-detail-header">
            <h1 class="title">{product.product_name}</h1>
                <div class="detail-footer">
                    <div class="price">
                        <div class="current-price">$ {product.product_price}</div>
                    </div>
                </div>
                <Link href="/cart_page">
                <button class="btn btn-primary btn-lg btn-block">
                    <span><i class="fas fa-shopping-cart fa-1x pr-2"></i></span>
                    Buy Now
                </button></Link>
            
         </div>
         </div>
         </div>
         </>
     )
} 

export default SingleProductPage;