import { withIronSession } from "next-iron-session";


export default withIronSession(
  async (req, res) => {
    if (req.method === "POST") {
      const { user} = req.body;
        // const user = req.session.get("user");
        req.session.set("user", { user: user });
        await req.session.save();
        return res.status(201).send({});
    
    }

    return res.status(404).send("");
  },
  {
    cookieName: "MYSITECOOKIE",
    cookieOptions: {
      secure: process.env.NODE_ENV === "production" ? true : false
    },
    password: process.env.NEXT_PUBLIC_PASSWORD_IRON_SESSION
  }
);