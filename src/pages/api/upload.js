const fs = require('fs');
const formidable = require("formidable")
const path  = require('path');
const slugify = require("slugify");
import product from "../../util/product"
export const config = {
    api : {
        bodyParser : false
    }
}

export default async (req,res) =>{
    fs.mkdir(`./public/upload`,{recursive:true},function(err){
       console.log(err)
    })
    const data = await new Promise((resolve,reject) =>{
        const form = formidable({
            multiples:true,
            uploadDir : `./public/upload`
        });
        form.keepExtensions = true;
        form.keepFileName = true;

        form.on('fileBegin',function(name,file) {
            file.path = path.join(`public/upload`,slugify(file.name))
        })
        form.parse(req,(err,fields,files) => {
            if(err) return reject(err);
            var path = [];
            if (Array.isArray(files.media)){
                files.media.forEach((data)=>{
                    path.push(data.path);
                })
                var new_path = path.toString();
                product.add_product(new_path,fields.product_name,fields.product_price)
                
            }else{
                product.add_product(files.media.path,fields.product_name,fields.product_price)
            }
            product.get_product()
            resolve(files)
        })
    })
   
    res.json(data);
}