import { withIronSession } from "next-iron-session";


export default withIronSession(
  async (req, res,session) => {
        req.session.destroy();
        return  res.send("Logged out"); 

  },
  {
    cookieName: "MYSITECOOKIE",
    cookieOptions: {
      secure: process.env.NODE_ENV === "production" ? true : false
    },
    password: process.env.NEXT_PUBLIC_PASSWORD_IRON_SESSION
  }
);