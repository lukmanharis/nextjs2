import React from "react";
import "styles/global.scss";
import "../../public/assets/css/style.css";
import Signupform from "components/Signup";
import NavbarCustom from "components/NavbarCustom";
function signupPage(props){
return(
    <>
    <NavbarCustom 
    bg="white"
    variant="light"
    expand="md"
    logo="https://uploads.divjoy.com/logo.svg"
    addbtn= "btn d-none"
    arrow = "d-flex w-100"
    btntoggle = "border-0 d-none"
    middletext = "Authenticate"
    />
    <Signupform />
    </>
)
}

export default signupPage;