import React from "react";
import VerifySection from "components/PhoneVerify";
import { useRouter } from "next/router";
import NavbarCustom from "components/NavbarCustom";
function VerifyPage(props) {
  const router = useRouter();
  return (
    <>
    <NavbarCustom 
       bg="white"
       variant="light"
       expand="md"
       logo="https://uploads.divjoy.com/logo.svg"
       addbtn= "btn d-none"
       arrow = "d-flex w-100"
       btntoggle = "border-0 d-none"
       middletext = "Verify"
    />
    <VerifySection
      user={router.query.user}
    />
    </>
  );
}

export default VerifyPage;
