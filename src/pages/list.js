import React from "react";
import Link from "next/link";
import "styles/global.scss";
import "../../public/assets/css/style.css";
import NavbarCustom from "components/NavbarCustom";
import { withIronSession } from "next-iron-session";
import product from "../util/product";
import localStorage from "../util/LocalStorage";
export const getServerSideProps = withIronSession(
  async ({ req, res }) => {
    const user = req.session.get("user");
    
    if (!user) {
      return { props: {} };
    }

    return {
      props: { user }
    };
  },
  {
    cookieName: "MYSITECOOKIE",
    cookieOptions: {
      secure: process.env.NODE_ENV === "production" ? true : false
    },
    password: process.env.NEXT_PUBLIC_PASSWORD_IRON_SESSION
  }
);
function ListPage(props) { 
  product.get_product();
  var data_list = [];
  if (typeof window !== "undefined") {
    data_list = localStorage.get_products();
  }
  return (
      <>
      <NavbarCustom 
      bg="grey"
      variant="light"
      expand="md"
      logo="https://uploads.divjoy.com/logo.svg"
      addbtn= "btn d-block"
      arrow = "d-none"
      btntoggle = "border-0"
      middletext = ""
      user = {props.user ? props.user : undefined}
    />
      <div id="listProducts">
        <h1 className ="title-list">My Products</h1>
        <p className ="subtitle-list">Welcome to My Products</p>
          <div class="listview-title mt-2">All products</div>
          <ul class="listview image-listview media mb-2">
              {data_list && data_list.map((item, index) => 
              <li>
                  <a href="#" onClick={function(){ product.add_single_product(item.id); setTimeout(function(){location.href="/single_product/"},500) }} class="item">
                      <div class="imageWrapper">
                      {item.data.product_image.split(",",1).map((image)=>
                        <img src={image.substr(7)} alt="Image" class="imaged w64"></img>
                        )}
                      </div>
                  <div class="in">
                      <div>
                        {item.data.product_name}
                        <div class="text-muted">${item.data.product_price}</div>
                      </div>
                  </div>
                  </a>
              </li>
              )
            }
          </ul>
      </div>
            </>
  );
}

export default ListPage;
