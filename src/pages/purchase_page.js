import React from "react";
import { useRouter } from 'next/router'
import Link from "next/link";
import product from "../util/product";
import localStorage from "../util/LocalStorage";
import "styles/global.scss";
import "../../public/assets/css/style.css";
import purchase from "util/purchase";
import Moment from 'moment';

function notificationPopUp() {
    var addNotification;
    addNotification = document.getElementById('saveNotification');
    addNotification.classList.add("show");
}

function closeNotificationBox() {
    var closeNotification;
    closeNotification  = document.getElementById('saveNotification');
       closeNotification.classList.remove("show")
}

function purchasePage (){
    var data_list = [];
    if (typeof window !== "undefined") {
      data_list = localStorage.get_products();
      console.log(data_list);
    }
  
    var total_amount = 0;
    data_list.map((item, index) => {
    total_amount += parseFloat(item.data.product_price);
    })

    const router = useRouter()
    var created_at = Moment(new Date()).format("Do MMMM YYYY");    

    return (
        <>
        <div class="appHeader bg-white text-dark">
        <div class="left">
        <div class="pageTitle">Purchase Complete</div>
        </div>
        <div class="right">
        <a href="/" class="headerButton-primary">
            <ion-icon name="home-sharp"></ion-icon>
        </a>
        </div>
        </div>

        <div class="bg-purchase mg-showorder-purchase">
                <ul class="listview flush transparent simple-listview">
                    <li>Show Order Summary<span class="text-primary font-weight-bold">$ {total_amount}</span></li>
                </ul>
        </div>

        <div class="pd-purchase bg-purchase mg-title-purchase">
        <h3 class="title-purchase">Order Details</h3>
        <p class="mg-p-purchase">Order ID: {router.query.id} </p>
        <p>Order Date: {created_at} </p> 
        </div>
        <div class="pd-purchase bg-purchase mg-title-purchase">
        <h3 class="title-purchase">Contact Information</h3>
        <p class="mg-p-purchase">Email: testing@gmail.com</p>
        <p>Phone: +65 1234 5678</p>
        </div>

        <div>
        <h3 class="subtitle-purchase pd-purchase">Enter Shipping Adress</h3>
        </div>
        <div class="form-group boxed pd-purchase bg-purchase">
                <div class="input-wrapper pd-purchase">
                    <label class="label" for="name1">First Name</label>
                    <input type="email" class="form-control" id="name1" placeholder="Enter your first name" required=""></input>
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter a valid e-mail.</div>
                

                    <label class="label" for="name2">Last Name</label>
                    <input type="text" class="form-control" id="name2" placeholder="Enter your last name" required=""></input>
                        <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter your phone number.</div>
                

                    <label class="label" for="address1">Address</label>
                    <input type="text" class="form-control" id="address1" placeholder="Enter your address" required=""></input>
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter your card information.</div>
                

                    <label class="label" for="apartment1">Apartment, suite</label>
                    <input type="email" class="form-control" id="apartment1" placeholder="Enter your apartment, suite" required=""></input>
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter a valid e-mail.</div>
                

                    <label class="label" for="city1">City</label>
                    <input type="text" class="form-control" id="city1" placeholder="Enter your city" required=""></input>
                        <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter your phone number.</div>

                    <label class="label" for="country1">Country</label>
                    <input type="text" class="form-control" id="country1" placeholder="Enter your country" required=""></input>
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter your card information.</div>
                

                    <label class="label" for="postalcode1">Postal Code</label>
                    <input type="text" class="form-control" id="postalcode1" placeholder="Enter your postal code" required=""></input>
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter your card information.</div>
                </div>
                </div>

                <div class="pd-purchase">
                <h3 class="subtitle-purchase">Order Notes</h3>
                </div>
                <div class="form-group boxed bg-purchase">
                <div class="input-wrapper pd-purchase">
                    <label class="label" for="ordernote1">Order Notes</label>
                    <input type="text" class="form-control" id="ordernote1" placeholder="Order Notes" required=""></input>
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter your card information.</div>
                </div>
                </div>

                <div class="mt-2">
                    <button class="btn btn-primary btn-block" type="submit" onClick={notificationPopUp}>Confirm Shipping Information</button>
                </div>

                <div id="saveNotification" class="notification-box">
                    <div class="notification-dialog ios-style">
                        <div class="notification-header">
                            <div class="in">
                                <img src="assets/img/greybackground.png" alt="image" class="imaged w24 rounded"></img>
                                <strong>Update</strong>
                            </div>
                            <div class="right">
                                <span>just now</span>
                                <a href="#" onClick={closeNotificationBox} class="close-button">
                                    <ion-icon name="close-circle"></ion-icon>
                                </a>
                            </div>
                        </div>
                        <div class="notification-content">
                            <div class="in">
                                <h3 class="subtitle">You have updated your shipping address</h3>
                            </div>
                        </div>
                        </div>
                        </div>
        </>
    )
}

export default purchasePage; 

  