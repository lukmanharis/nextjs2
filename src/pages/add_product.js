import React ,{useEffect,useState}from "react";
import "styles/global.scss";
import "../../public/assets/css/style.css";
import axios from 'axios';
import product from "../util/product";

function notification() {
    var addNotification;
    addNotification = document.getElementById('productNotification');
    addNotification.classList.add("show");
}

function closeNotificationBox() {
    var closeNotification;
    closeNotification  = document.getElementById('productNotification');
       closeNotification.classList.remove("show")
}

function addProductPage(props){
    var i = 0;
    useEffect(() => {
        var imagesPreview = function(input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;
                console.log(filesAmount);
                for (var i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img class="mr-3" height="100px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
                    
                    reader.readAsDataURL(input.files[i]);
                }
            }
    
        };
        $('#fileuploadInput').on("change",function(e){
            $("div.image-display").empty();
            imagesPreview(this,'.image-display')
        });
    })
    const add_product = async (event) => {
        event.preventDefault();
        var btn = document.getElementById("btn-submit");
        var proggres_bar = document.getElementById("myProgress");
        btn.classList.remove("btn-primary");
        btn.classList.add("btn-secondary");
        proggres_bar.classList.remove("d-none");
        btn.innerHTML = "Adding.......";
        btn.setAttribute("disabled","disabled")
        
        if (i == 0) {
            i = 1;
            var elem = document.getElementById("myBar");
            var width = 10;
            var id = setInterval(frame, 10);
            function frame() {
              if (width >= 100) {
                clearInterval(id);
                i = 0;
                btn.classList.remove("btn-secondary");
                btn.classList.add("btn-primary");
                proggres_bar.classList.add("d-none");
                btn.innerHTML = "Add Product";
                btn.removeAttribute("disabled");
                notification();
              } else {
                width++;
                elem.style.width = width + "%";
                elem.innerHTML = width  + "%";
              }
            }
          }
        var image = document.getElementById("fileuploadInput");
        var product_name = document.getElementById("product_name");
        var product_price = document.getElementById("product_price");
        var form = new FormData();
        console.log(image.files.length)
        if (image.files.length > 0){
            image.files.forEach((file)=> form.append("media",file))
        }
        form.append("product_name", product_name.value)
        form.append("product_price",product_price.value)
        await axios('/api/upload',{
            method: "POST",
            data: form,
            "content-type": "multipart/form-data"
        }).then((res)=>{
            if(res.status == 200){

            }
        })
      };
    return (
        <>
        <div class="appHeader bg-primary text-light">
            <div class="left">
                <a href="#" class="headerButton goBack">
                    <span><i class="fas fa-chevron-left fa-1x"></i></span>
                </a>
            </div>
        <div class="pageTitle">Add Product</div>
        </div>
        <div class="section full mt-2 mb-2">
            <div class="section-title">Enter the following information</div>
                <div class="wide-block pt-2 pb-2">
                    <form class="needs-validation" noValidate="">
                        <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" class="form-control" id="product_name" placeholder="Title" required=""></input>
                            <i class="clear-input">
                                <span><i class="fas fa-times-circle fa-3x"></i></span>
                            </i>
                            <div class="valid-feedback">Looks good!</div>
                            <div class="invalid-feedback">Please enter your product title.</div>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="number" class="form-control" id="product_price" placeholder="Price" required=""></input>
                            <i class="clear-input">
                                <span><i class="fas fa-times-circle fa-3x"></i></span>
                            </i>
                            <div class="valid-feedback">Looks good!</div>
                            <div class="invalid-feedback">Please enter a valid price.</div>
                        </div>
                    </div>
                    <div class="section full">
                    <div class="upload-block">
                        <form> 
                            <div className="image-display d-inline-flex mb-2">
                            </div>
                            <div className="custom-file-upload" id="fileUpload1">
                                <input type="file" id="fileuploadInput" name="myImage" multiple accept=".png, .jpg, .jpeg"></input>
                                    <label for="fileuploadInput">
                                        <span>
                                            <strong>
                                        <span><i class="fas fa-cloud-upload-alt fa-2x"></i></span>
                                            <i className="btn-upload-text">Tap to Upload Photos</i>
                                            </strong>
                                        </span>
                                    </label>
                            </div>
                        </form>
                    </div>
                    </div>
                    <div class="mt-2">
                        <div id="myProgress" className="mb-1 d-none">
                            <div id="myBar">10%</div>
                        </div>
                            <button class="btn btn-primary btn-block" type="submit" id="btn-submit" onClick={add_product}>Add Product</button>
                    </div>
        
                    <div id="productNotification" class="notification-box">
                    <div class="notification-dialog ios-style">
                        <div class="notification-header">
                            <div class="in">
                                <img src="assets/img/greybackground.png" alt="image" class="imaged w24 rounded"></img>
                                <strong>Product Added</strong>
                            </div>
                            <div class="right">
                                <span>just now</span>
                                <a href="#" onClick={closeNotificationBox} class="close-button">
                                    <ion-icon name="close-circle"></ion-icon>
                                </a>
                            </div>
                        </div>
                        <div class="notification-content">
                            <div class="in">
                                <h3 class="subtitle">Your product is added</h3>
                                <div class="text">
                                    What do you want to do next?
                                </div>
                            </div>
                        </div>
                        <div class="notification-footer">
                            <a href="#" onClick={function(){ product.get_product(); location.reload()}} class="notification-button">
                                Add Product
                            </a>
                            <a href="#" onClick={function(){product.get_product(); location.href="/list"}} class="notification-button">
                                See My Products
                            </a>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        </>
    ) 
}

export default addProductPage;

