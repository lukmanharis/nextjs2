import React from "react";
import firebase from "firebase/app";
import product from "../util/product";
import localStorage from "../util/LocalStorage";
import "styles/global.scss";
import "../../public/assets/css/style.css";

function discountCode (){
    var discount;
     discount = document.getElementById('actionSheetDiscount');
     discount.classList.add("show");
     console.log('test')
}

function cartPage (){
product.get_product();
  var data_list = [];
  if (typeof window !== "undefined") {
    data_list = localStorage.get_products();
    console.log(data_list);
  }

  var total_price = 0;
  data_list.map((item, index) => {
      total_price += parseFloat(item.data.product_price);
  })

    return (
    <>
    <div class="appHeader bg-primary text-light">
        <div class="left">
            <a href="/single_product" class="headerButton goBack">
                <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">Cart</div>
        <div class="right">
            <a href="#" class="headerButton" data-bs-toggle="modal" data-bs-target="#DialogClear">
                <ion-icon name="trash-outline" role="img" class="md hydrated" aria-label="trash outline"></ion-icon>
            </a>
        </div>
    </div>
    <div id="appCapsule">

        <div class="section mt-2">
        {data_list && data_list.map((item, index) => 
            <div class="card cart-item mb-2">
                <div class="card-body">
                    <div class="in">
                        {item.data.product_image.split(",",1).map((image)=>
                        <img src={image.substr(7)} alt="Image" class="imaged w64"></img>
                        )}
                        <div class="text">
                            <h3 class="title">{item.data.product_name}</h3>
                            <p class="detail">1 piece</p>
                            <strong class="price">$ {item.data.product_price}</strong>
                        </div>
                    </div>
                    <div class="cart-item-footer">
                        <div class="stepper stepper-sm stepper-secondary">
                            <a class="stepper-button stepper-down" onClick={function(){
                                var input = document.getElementsByClassName("input-"+item.id)[0];
                                if (!(input.value -1 <= 0)){ 
                                    input.value--
                                }
                            }}>-</a>
                            <input id="amount" type="text" class={"form-control input-"+item.id} value="1" readOnly=""></input>
                            <a class="stepper-button stepper-up" onClick={function(){
                                var input = document.getElementsByClassName("input-"+item.id)[0];
                                input.value++
                            }}>+</a>
                        </div>
                        <a href="#" class="btn btn-outline-secondary btn-sm" onClick={() => {product.remove_product(item.id);}}>Delete</a>
                    </div>
                </div>
            </div>
            )}
        </div>

        <div class="section">
            <a href="#" class="btn btn-sm btn-text-secondary btn-block" data-bs-toggle="modal" data-bs-target="#actionSheetDiscount" onClick={discountCode}>
                <ion-icon name="qr-code-outline" role="img" class="md hydrated" aria-label="qr code outline"></ion-icon>
                Have a discount code?
            </a>
        </div>

        <div class="modal fade action-sheet" id="actionSheetDiscount">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Enter Discount Code</h5>
                    </div>
                    <div class="modal-body">
                        <div class="action-sheet-content">
                            <form>
                                <div class="form-group basic">
                                    <div class="input-wrapper">
                                        <label class="form-label" for="discount1">Discount Code</label>
                                        <input type="text" class="form-control" id="discount1" placeholder="Enter your discount code"></input>
                                        <i class="clear-input">
                                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                                        </i>
                                    </div>
                                </div>

                                <div class="form-group basic">
                                    <button type="button" class="btn btn-primary btn-block" data-bs-dismiss="modal">Apply
                                        Discount</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section mt-2 mb-2">
            <div class="card">
                <ul class="listview flush transparent simple-listview">
                    <li>Total<span class="text-primary font-weight-bold">$ {total_price}</span></li>
                </ul>
            </div>
        </div>

        <div class="section mb-2">
            <a href="checkout_page" class="btn btn-primary btn-block ">Order Now</a>
        </div>
    </div>
    </>
    )
}

export default cartPage;
