import React from "react";
import "styles/global.scss";
import "util/analytics.js";
import "../../public/assets/css/style.css";
import { AuthProvider } from "util/auth.js";
import Head from 'next/head';
function MyApp({ Component, pageProps }) {
  return (
    <AuthProvider>
      <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, viewport-fit=cover"
        ></meta>
        <script src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.js"></script>  
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      </Head>
        <Component {...pageProps} />
      </>
    </AuthProvider>
  );
}
export default MyApp;