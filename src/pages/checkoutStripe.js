import Head from "next/head";
import React from "react";
import { GetServerSideProps } from "next";
import { loadStripe } from "@stripe/stripe-js";
import Stripe from "stripe";
import { async } from "regenerator-runtime";
import { setCookie, parseCookies } from "nookies";
import { Elements } from "@stripe/react-stripe-js";
import CheckoutForm from "../components/CheckoutForm";

const stripePromise = loadStripe("pk_test_51J6jlRDf7AV3vT7nwyS3c0mRMpSR885jUAE5bcIgvBKz6pDTU5Uh1DEqTaiCOmmslVn1PHZ4H85BbaqOWOn2fSZo00Voglo48M");

const checkoutStripe = ({ paymentIntent }) => {
  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm paymentIntent={paymentIntent} />
    </Elements>
  );
};

export default checkoutStripe;

export const getServerSideProps = async (ctx) => {
  const stripe = new Stripe("sk_test_51J6jlRDf7AV3vT7nwSHT9SsiUtbmKrExjEjuBaOcnrqC9RJWEPwOZLmutsFlgHBQOgJIRQP8lg9SM35c766MQDMi00iFVe4Kkp");
  let paymentIntent;
  const { paymentIntentId } = await parseCookies(ctx);
  if (paymentIntentId) {
    paymentIntent = await stripe.paymentIntents.retrieve(paymentIntentId);
    return {
      props: {
        paymentIntent,
      },
    };
  }

  paymentIntent = await stripe.paymentIntents.create({
    setup_future_usage: "off_session",
    amount: 1000,
    currency: "sgd",
    payment_method_types: ["card"],
  });

  setCookie(ctx, "paymentIntentId", paymentIntent.id);
  const token = await stripe.tokens.create({
    card: {
      number: "4242424242424242",
      exp_month: 6,
      exp_year: 2022,
      cvc: "314",
    },
  });
  return {
    props: {
      paymentIntent,
    },
  };
};
