import React from "react";
import Link from "next/link";
import "styles/global.scss";
import "../../public/assets/css/style.css";

function saveInformationPage (props){
    return (
        <>
        <div class="appHeader bg-white text-dark">
        <div class="left">
            <a href="#" class="headerButton goBack">
                <ion-icon name="chevron-back-outline" role="img" class="md hydrated" aria-label="chevron back outline"></ion-icon>
            </a>
            <div class="pageTitle">Save Card Information</div>
        </div>
        </div>

            <div class="section full mt-2 mb-2">
                <div class="wide-block pt-2 pb-2">
                    <form class="needs-validation" novalidate="">
                        <h2 style={{marginTop: '30px'}}>Thanks for verifying!</h2>
                            <p style={{color: 'black'}}>Save your payment information to complete checkout. You can still view your shopping cart in the next step.</p>
                    
                <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label" for="email1">Email</label>
                    <input type="email" class="form-control" id="email1" placeholder="Enter your email adress" required=""></input>
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter a valid e-mail.</div>
                </div>
                </div>

                <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label" for="number1">Primary Phone Number</label>
                    <input type="text" class="form-control" id="number1" placeholder="Enter your phone number" required=""></input>
                        <i class="clear-input">
                                <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter your phone number.</div>
                </div>
                </div>

                <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label" for="card1">Card Information</label>
                    <input type="text" class="form-control" id="card1" placeholder="Enter your card information" required=""></input>
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter your card information.</div>
                </div>
                </div>

                <div class="form-group boxed" style={{flexDirection: 'row', display: 'flex', width: '98%'}}>
                <div class="form-row" style={{marginLeft: '1px'}}>
                <div class="form-group">
                    <label for="inputExpiry5">Expiry Date</label>
                    <input type="text" class="form-control" id="inputExpiry5" placeholder=""></input>
                </div>
                </div>

                <div class="form-row" style={{marginLeft: '20px'}}>
                <div class="form-group">
                    <label for="inputCvv5">CVV</label>
                    <input type="text" class="form-control" id="inputCvv5" placeholder=""></input>
                </div>
                </div>
                </div>
                
                <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label" for="name1">Name on card</label>
                    <input type="text" class="form-control" id="name1" placeholder="Enter your name" required=""></input>
                        <i class="clear-input">
                            <ion-icon name="close-circle" role="img" class="md hydrated" aria-label="close circle"></ion-icon>
                        </i>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please enter your name.</div>
                </div>
                </div>

                <div class="form-group boxed">
                <div class="input-wrapper">
                    <label class="label" for="country1">Country</label>
                        <select class="form-control custom-select" id="country1" required="">
                            <option selected="" disabled="" value="">Choose...</option>
                            <option value="1">Singapore</option>
                            <option value="2">Malaysia</option>
                            <option value="3">Indonesia</option>
                        </select>
                <div class="valid-feedback">Looks good!</div>
                <div class="invalid-feedback">Please choose a country.</div>
                </div>
                </div>

                <div class="mt-2">
                    <button class="btn btn-primary btn-block" type="submit">Save Card</button>
                </div>

                <div onClick="" style={{textAlign: 'center', marginTop: '5px'}}>Skip for now</div>
                    </form>
                </div>
            </div>
        </>
    )
}

export default saveInformationPage ;