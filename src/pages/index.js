import React from "react";
import HeroSection from "components/HeroSection";
import NavbarCustom from "components/NavbarCustom";
import { withIronSession } from "next-iron-session";
import product from "../util/product"
import localStorage from "../util/LocalStorage";
export const getServerSideProps = withIronSession(
  async ({ req, res }) => {
    const user = req.session.get("user");
    
    if (!user) {
      return { props: {} };
    }

    return {
      props: { user }
    };
  },
  {
    cookieName: "MYSITECOOKIE",
    cookieOptions: {
      secure: process.env.NODE_ENV === "production" ? true : false
    },
    password: process.env.NEXT_PUBLIC_PASSWORD_IRON_SESSION
  }
);

function IndexPage(props) {
  product.get_product();
  var data_list = [];
  var product_count;
  if (typeof window !== "undefined") {
    data_list = localStorage.get_products();
    if (data_list != null ){
    product_count = data_list.length;
    }
  }
  return (
    <>
    <NavbarCustom 
      bg="white"
      variant="light"
      expand="md"
      logo="https://uploads.divjoy.com/logo.svg"
      addbtn= "btn d-none"
      arrow = "d-none"
      btntoggle = "border-0"
      middletext = ""
      user = {props.user ? props.user : undefined}
      >
      </NavbarCustom>
      <HeroSection
        bg="white"
        textColor="dark"
        size="md"
        bgImage=""
        bgImageOpacity={1}
        title="Home"
        subtitle="Welcome to Products Projects"
        image="https://uploads.divjoy.com/undraw-japan_ubgk.svg"
        buttonText="Get Started"
        buttonColor="primary"
        buttonPath="/pricing"
        user = {props.user ? props.user : undefined}
        product_count = {product_count}
      />
      <div id="listProducts">
          <div class="listview-title mt-2 font-weight-bold">All products (from anyone)</div>
          <ul class="listview image-listview media mb-2">
              {data_list && data_list.map((item, index) => 
              <li>
                  <a href="#" onClick={function(){ product.add_single_product(item.id); setTimeout(function(){location.href="/single_product/"},500) }} class="item">
                      <div class="imageWrapper">
                      {item.data.product_image.split(",",1).map((image)=>
                        <img src={image.substr(7)} alt="Image" class="imaged w64"></img>
                        )}
                      </div>
                  <div class="in">
                      <div>
                        {item.data.product_name}
                        <div class="text-muted">${item.data.product_price}</div>
                      </div>
                  </div>
                  </a>
              </li>
              )
            }
          </ul>
      </div>
    </>
  );
}

export default IndexPage;
